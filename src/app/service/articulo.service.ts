import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Articulo } from '../private/models/articulo.model';
import { ArticuloRequest } from '../private/models/articulo-request.model';

@Injectable({
  providedIn: 'root',
})
export class ArticuloService {
  URL: string = 'http://localhost:9090/articulo/';
  articulo!: Articulo;

  constructor(private httpClient: HttpClient) {}

  getArticulos<ArticuloList>(
    npag: number,
    rxpag: number
  ): Observable<ArticuloList> {
    const httpParams = new HttpParams({
      fromObject: {
        page: npag,
        size: rxpag,
        enablePagination: true,
      },
    });
    return this.httpClient.get<ArticuloList>(this.URL, { params: httpParams });
  }
  getArticulo<Articulo>(idArticulo: number): Observable<Articulo> {
    return this.httpClient.get<Articulo>(this.URL + `${idArticulo}`);
  }

  postArticulos(articuloRequest: ArticuloRequest): Observable<Articulo> {
    return this.httpClient.post<Articulo>(this.URL, articuloRequest);
  }

  putArticulo(articulo: Articulo): Observable<Articulo> {
    return this.httpClient.put<Articulo>(this.URL, articulo);
  }

  deelteArticulo<Articulo>(idArticulo: number): Observable<Articulo> {
    return this.httpClient.delete<Articulo>(this.URL + `${idArticulo}`);
  }
}
