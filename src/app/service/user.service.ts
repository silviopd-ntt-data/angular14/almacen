import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserRequest } from '../private/models/user-request.model';
import { User } from '../private/models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  URL: string = 'http://localhost:8080/api/auth/';
  user!: User;

  constructor(private httpClient: HttpClient) {}

  getUsers<UserList>(npag: number, rxpag: number): Observable<UserList> {
    const httpParams = new HttpParams({
      fromObject: {
        page: npag,
        size: rxpag,
        enablePagination: true,
      },
    });
    return this.httpClient.get<UserList>(this.URL, { params: httpParams });
  }
  getUser<User>(id: number): Observable<User> {
    return this.httpClient.get<User>(this.URL + `${id}`);
  }

  postUsers(UserRequest: UserRequest): Observable<User> {
    return this.httpClient.post<User>(this.URL + 'signup', UserRequest);
  }

  putUser(User: any): Observable<any> {
    return this.httpClient.post<User>(this.URL + 'update', User);
  }
}
