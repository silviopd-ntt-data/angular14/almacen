import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  constructor(private router: Router, private cookieService: CookieService) {}

  ngOnInit(): void {}

  username: string = this.cookieService.get('username');

  logOut() {
    this.cookieService.deleteAll();
    // this.cookieService.delete('token_access', '/');
    // this.cookieService.delete('username', '/');
    location.reload();
    // this.router.navigate(['/login']);
  }
}
