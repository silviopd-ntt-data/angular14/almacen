import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/service/login.service';
import { CookieService } from 'ngx-cookie-service';
import { CookieResult } from 'src/app/private/models/cookie-result';
import { Login } from 'src/app/private/models/login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginAuthentication: FormGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
  });

  constructor(
    private fromBuilder: FormBuilder,
    private loginService: LoginService,
    private rotuer: Router,
    private cookieService: CookieService
  ) {
    this.loginAuthentication = this.fromBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {}
  autenticarLogin() {
    if (this.loginAuthentication.invalid) return;

    const login: Login = {
      username: this.loginAuthentication.value.username,
      password: this.loginAuthentication.value.password,
    };
    this.loginService.postAuthenticate(login).subscribe((e: CookieResult) => {
      console.log(e);
      this.cookieService.set('token_access', e.accessToken, 1);
      this.cookieService.set('username', e.username, 1);
      this.rotuer.navigate(['/prd/listar']);
    });
  }
}
