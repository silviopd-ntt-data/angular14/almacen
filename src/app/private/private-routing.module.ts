import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticuloListComponent } from './articulo/articulo-list/articulo-list.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { ProductCreateComponent } from './product/product-create/product-create.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductViewComponent } from './product/product-view/product-view.component';
import { ProductComponent } from './product/product.component';
import { AuthGuard } from '../guards/auth.guard';
import { UserComponent } from './user/user.component';
import { UserListComponent } from './user/user-list/user-list.component';

const routes: Routes = [
  {
    path: 'prd',
    component: ProductComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'listar',
        component: ProductListComponent,
      },
      {
        path: 'editar/:id',
        component: ProductEditComponent,
      },
      { path: 'ver/:id', component: ProductViewComponent },
      { path: 'crear', component: ProductCreateComponent },
      { path: '**', component: ProductListComponent },
    ],
  },
  {
    path: 'articulo',
    component: ArticuloComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'listar', component: ArticuloListComponent },
      { path: '**', component: ArticuloListComponent },
    ],
  },
  {
    path: 'user',
    component: UserComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'listar', component: UserListComponent },
      { path: '**', component: UserListComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivateRoutingModule {}
