import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit, Optional } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { Role } from '../../models/role.model';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css'],
})
export class UserDialogComponent implements OnInit {
  // public id: number,
  //   public username: string,
  //   public email: string,
  //   public password: string,
  // public role: Role[]
  toppingList: string[] = ['admin', 'mod', 'user'];
  listRoles: Role[] = [];

  crearEditarUser: FormGroup = new FormGroup({
    id: new FormControl(),
    username: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    roles: new FormControl(),
  });

  view = false;

  title: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private UserService: UserService,
    private router: Router,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<UserDialogComponent>
  ) {
    this.crearEditarUser = formBuilder.group({
      id: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      roles: ['', Validators.required],
    });

    console.log(this.data);

    this.title =
      this.data.action === 'nuevo' ? 'Crear Usuario' : 'Editar Usuario';

    if (this.data.action == 'ver') {
      this.view = true;
      this.title = 'Ver Usuario';
    }
  }

  ngOnInit(): void {
    if (this.data.data != null) {
      this.UserService.getUser<User>(this.data.data.id).subscribe((res) => {
        this.crearEditarUser = this.formBuilder.group({
          id: [res.id, Validators.required],
          username: [res.username, Validators.required],
          email: [res.email, Validators.required],
          password:
            this.data.action === 'editar'
              ? ''
              : [(res.password, Validators.required)],
          roles: [res.roles, Validators.required],
        });

        this.listRoles = [...res.roles];
      });
    }
  }

  doAction() {
    this.dialogRef.close({
      data: {
        id: this.crearEditarUser.value.id,
        username: this.crearEditarUser.value.username,
        email: this.crearEditarUser.value.email,
        password: this.crearEditarUser.value.password,
        role: this.crearEditarUser.value.roles,
      },
      action: this.data.action,
    });
  }

  closeDialog() {
    this.dialogRef.close({ data: null, action: 'cancel' });
  }
}
