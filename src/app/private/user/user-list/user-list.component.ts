import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { UserList } from '../../models/user-list.model';
import { UserRequest } from '../../models/user-request.model';
import { User } from '../../models/user.model';
import { UserDialogComponent } from '../user-dialog/user-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  listaUsers: User[] = [];
  np: number = 0;
  rp: number = 5;
  tPages: number = 0;
  tElements: number = 0;

  pageSizeOptions: number[] = [5, 10, 25, 30];

  displayedColumns: string[] = [
    'id',
    'username',
    'email',
    'password',
    'role',
    'actions',
  ];

  handlePages(e: PageEvent) {
    this.tElements = e.length;
    this.rp = e.pageSize;
    this.np = e.pageIndex;
    this.cargarUsers();
  }

  constructor(
    private service: UserService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.cargarUsers();
  }

  cargarUsers() {
    this.service.getUsers<UserList>(this.np, this.rp).subscribe((res) => {
      this.listaUsers = res.content;
      this.tPages = res.totalPages;
      this.tElements = res.totalElements;
    });
  }

  crearUsers(data: UserRequest) {
    console.log(data);
    this.service.postUsers(data).subscribe((res) => {
      this.cargarUsers();
    });
  }

  editarUser(data: User) {
    this.service.putUser(data).subscribe((res) => {
      this.cargarUsers();
    });
  }

  // eliminarUser(data: User) {
  //   this.service.deleteUser(data.id).subscribe((res) => {
  //     this.cargarUsers();
  //   });
  // }

  cambiarPagina(varlor: number) {
    this.np += varlor;
    if (this.np < 0) this.np = 0;
    if (this.np >= this.tPages) this.np = this.tPages - 1;
    this.cargarUsers();
  }

  openDialog(action: any, data: any) {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      data: {
        data: data,
        action: action,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.action == 'editar') {
          this.editarUser(result.data);
        }

        if (result.action == 'nuevo') {
          this.crearUsers(result.data);
        }
      }
    });
  }
}
