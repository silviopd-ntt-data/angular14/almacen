import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProductCreateComponent } from './product/product-create/product-create.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { ProductViewComponent } from './product/product-view/product-view.component';
import { ProductComponent } from './product/product.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from '../shared/sidebar/sidebar.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { ContentHeaderComponent } from '../shared/content-header/content-header.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { ArticuloListComponent } from './articulo/articulo-list/articulo-list.component';
import { ArticuloDialogComponent } from './articulo/articulo-dialog/articulo-dialog.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserDialogComponent } from './user/user-dialog/user-dialog.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    ProductListComponent,
    ProductCreateComponent,
    ProductEditComponent,
    ProductViewComponent,
    ProductComponent,
    SidebarComponent,
    NavbarComponent,
    ContentHeaderComponent,
    ArticuloComponent,
    ArticuloListComponent,
    ArticuloDialogComponent,
    UserComponent,
    UserListComponent,
    UserDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
  ],
})
export class PrivateModule {}
