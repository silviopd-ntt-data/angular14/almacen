export class Articulo {
  constructor(
    public idArticulo: number,
    public nombre: string,
    public precio: number,
    public costo: number,
    public fecha: string,
    public descripcion: string
  ) {}
}
