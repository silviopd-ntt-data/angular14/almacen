export class CookieResult {
  constructor(
    public id: number,
    public accessToken: string,
    public username: string
  ) {}
}
