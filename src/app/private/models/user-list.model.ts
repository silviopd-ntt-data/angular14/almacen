import { Sort } from './sort.module';
import { User } from './user.model';

export class UserList {
  constructor(
    public content: User[],
    public pageable: string,
    public last: boolean,
    public totalPages: number,
    public totalElements: number,
    public size: number,
    public number: number,
    public sort: Sort,
    public first: boolean,
    public numberOfElements: number,
    public empty: boolean
  ) {}
}
