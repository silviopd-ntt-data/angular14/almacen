import { Product } from "./product.model";
import { Sort } from "./sort.module";

export class ProductList {
    constructor(
        public content: Product[],
        public pageable: string,
        public last: boolean,
        public totalPages: number,
        public totalElements: number,
        public size: number,
        public number: number,
        public sort: Sort,
        public first: boolean,
        public numberOfElements: number,
        public empty: boolean
        ) { }
}