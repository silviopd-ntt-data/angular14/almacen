import { Role } from './role.model';

export class UserRequest {
  constructor(
    public username: string,
    public email: string,
    public password: string,
    public role: Role[]
  ) {}
}
