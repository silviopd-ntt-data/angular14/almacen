export class ArticuloRequest {
  constructor(
    public nombre: string,
    public precio: number,
    public costo: number,
    public fecha: string,
    public descripcion: string
  ) {}
}
