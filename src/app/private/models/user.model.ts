import { Role } from './role.model';

export class User {
  constructor(
    public id: string,
    public username: string,
    public email: string,
    public password: string,
    public roles: Role[]
  ) {}
}
