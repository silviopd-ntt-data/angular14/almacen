import { Component, OnInit, Optional } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ArticuloService } from 'src/app/service/articulo.service';
import { Inject } from '@angular/core';
import { Articulo } from '../../models/articulo.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { ArticuloRequest } from '../../models/articulo-request.model';

@Component({
  selector: 'app-articulo-dialog',
  templateUrl: './articulo-dialog.component.html',
  styleUrls: ['./articulo-dialog.component.css'],
})
export class ArticuloDialogComponent implements OnInit {
  crearEditarArticulo: FormGroup = new FormGroup({
    idArticulo: new FormControl(),
    nombre: new FormControl(),
    precio: new FormControl(),
    costo: new FormControl(),
    fecha: new FormControl(),
    descripcion: new FormControl(),
  });

  view = false;
  title: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private articuloService: ArticuloService,
    private router: Router,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ArticuloDialogComponent>
  ) {
    this.crearEditarArticulo = formBuilder.group({
      idArticulo: ['', Validators.required],
      nombre: ['', Validators.required],
      precio: ['', Validators.required],
      costo: ['', Validators.required],
      fecha: ['', Validators.required],
      descripcion: ['', Validators.required],
    });

    console.log(this.data);
    this.title =
      this.data.action === 'nuevo' ? 'Crear Articulo' : 'Editar Articulo';
    if (this.data.action == 'ver') {
      this.view = true;
      this.title = 'Ver Articulo';
    }
  }

  ngOnInit(): void {
    if (this.data.data != null) {
      this.articuloService
        .getArticulo<Articulo>(this.data.data.idArticulo)
        .subscribe((res) => {
          let pipe = new DatePipe('en-US');
          this.crearEditarArticulo = this.formBuilder.group({
            idArticulo: [res.idArticulo, Validators.required],
            nombre: [res.nombre, Validators.required],
            precio: [res.precio, Validators.required],
            costo: [res.costo, Validators.required],
            descripcion: [res.descripcion, Validators.required],
            fecha: [
              pipe.transform(res.fecha, 'yyyy-MM-dd'),
              Validators.required,
            ],
          });
        });
    }
  }

  doAction() {
    this.dialogRef.close({
      data: {
        idArticulo: this.crearEditarArticulo.value.idArticulo,
        nombre: this.crearEditarArticulo.value.nombre,
        precio: this.crearEditarArticulo.value.precio,
        costo: this.crearEditarArticulo.value.costo,
        fecha: this.crearEditarArticulo.value.fecha,
        descripcion: this.crearEditarArticulo.value.descripcion,
      },
      action: this.data.action,
    });
  }

  closeDialog() {
    this.dialogRef.close({ data: null, action: 'cancel' });
  }
}
