import { Component, OnInit } from '@angular/core';
import { ChildrenOutletContexts, NavigationEnd, Router } from '@angular/router';
import { ArticuloService } from 'src/app/service/articulo.service';
import { ArticuloList } from '../../models/articulo-list.model';
import { Articulo } from '../../models/articulo.model';
import { PageEvent } from '@angular/material/paginator';
import { ArticuloDialogComponent } from '../articulo-dialog/articulo-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { filter, map } from 'rxjs';
import { ArticuloRequest } from '../../models/articulo-request.model';

@Component({
  selector: 'app-articulo-list',
  templateUrl: './articulo-list.component.html',
  styleUrls: ['./articulo-list.component.css'],
})
export class ArticuloListComponent implements OnInit {
  listaArticulos: Articulo[] = [];
  np: number = 0;
  rp: number = 5;
  tPages: number = 0;
  tElements: number = 0;

  pageSizeOptions: number[] = [5, 10, 25, 30];

  displayedColumns: string[] = [
    'idArticulo',
    'nombre',
    'precio',
    'costo',
    'fecha',
    'descripcion',
    'actions',
  ];

  handlePages(e: PageEvent) {
    this.tElements = e.length;
    this.rp = e.pageSize;
    this.np = e.pageIndex;
    this.cargarArticulos();
  }

  constructor(
    private service: ArticuloService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.cargarArticulos();
  }

  cargarArticulos() {
    this.service
      .getArticulos<ArticuloList>(this.np, this.rp)
      .subscribe((res) => {
        this.listaArticulos = res.content;
        this.tPages = res.totalPages;
        this.tElements = res.totalElements;
      });
  }

  crearArticulos(data: ArticuloRequest) {
    this.service.postArticulos(data).subscribe((res) => {
      this.cargarArticulos();
    });
  }

  editarArticulo(data: Articulo) {
    this.service.putArticulo(data).subscribe((res) => {
      this.cargarArticulos();
    });
  }

  eliminarArticulo(data: Articulo) {
    this.service.deelteArticulo(data.idArticulo).subscribe((res) => {
      this.cargarArticulos();
    });
  }

  cambiarPagina(varlor: number) {
    this.np += varlor;
    if (this.np < 0) this.np = 0;
    if (this.np >= this.tPages) this.np = this.tPages - 1;
    this.cargarArticulos();
  }

  openDialog(action: any, data: any) {
    const dialogRef = this.dialog.open(ArticuloDialogComponent, {
      data: {
        data: data,
        action: action,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.action == 'editar') {
          this.editarArticulo(result.data);
        }

        if (result.action == 'nuevo') {
          this.crearArticulos(result.data);
        }
      }
    });
  }
}
